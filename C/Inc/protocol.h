/*
MIT License

Copyright (c) 2023 Vigo Photonics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
@file protocol.h
Contains all structures used for communication with AMS-DIG-PROC board.
For more information about protocol check datasheet for AMS-DIG-PROC available on vigo.com.pl
*/

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include <stdint.h>

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#error __ORDER_BIG_ENDIAN__ is not supported!
#endif

#pragma pack(push, 1)

#define SAMPLES_BUFFER_SIZE_IN_BYTES 8192
#define USER_SPACE_SIZE 256
#define PROCESSING_SLOTS 4
#define SIZE_WITH_COBS_OVERHEAD(x) (2 + x + (x)/256)

#define ADC_REFERENCE_VOLTAGE (3.3) /// 3.3 Volts

/** Sample size definitions. */
typedef enum {
    SAMPLE_SIZE_8b = 1,
    SAMPLE_SIZE_16b = 2,
    SAMPLE_SIZE_32b = 4,
} sample_size_t;

typedef enum {
    EDGE_RISING = 1, /// Only rising edge is currently implemented
} edge_t;

typedef enum {
    PHYSICAL_RESOLUTION_16b = 2,
} physical_resolution_t;

typedef enum {
    PROCESSING_RESOLUTION_32b = 4,
} processing_resolution_t;

/** Types of samples with different sample sizes. */
typedef union {
    uint8_t data8[SAMPLES_BUFFER_SIZE_IN_BYTES];
    uint16_t data16[SAMPLES_BUFFER_SIZE_IN_BYTES / 2];
    uint32_t data32[SAMPLES_BUFFER_SIZE_IN_BYTES / 4];
} data_samples_t;

/** Header of all messages.*/
typedef struct {
    uint32_t crc;
    uint8_t msgId;
} message_header_t;

#define MESSAGE_GENERIC_PART \
        message_header_t header; \
        uint8_t end_of_header[0]; \

/** Generic message structure. Keep in mind that there is no payload included, but only pointer to the first byte of payload.*/
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t payload[0];  /// Useful for extracting pointer to start of payload.
} message_generic_t;



/** Periodic status message. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t reset_flag;
    uint8_t configuration_unsaved;
    uint8_t sampling_state;
    uint8_t processing_state;
    uint32_t data_overflow_counter;
    uint32_t messages_received_counter;
    uint32_t detector_temperature;
    uint8_t temperature_ok;
} message_status_t;

/** Clears reset flag of the board. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
} message_clear_reset_flag_t;

/** Restarts the board. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
} message_reboot_t;

/** Configures communication parameters. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
  uint32_t uart_baud;
} message_configure_communication_t;

/** Configures sampling parameters. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint32_t physicalSampleRate;
    uint8_t physicalResolution; /// One of physical_resolution_t values
    uint8_t processingResolution; /// One of processing_resolution_t values
} message_configure_sampling_t;

/** Sets temperature of the detector. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
  uint16_t temperature;
} message_configure_detector_temperature_t;

/** Sets user space data. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
  uint8_t data[USER_SPACE_SIZE];
} message_configure_user_space_t;

/** Stores config tu nonvolatile memory. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
} message_config_save_t;

/** Reads given current config setting. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
  uint8_t configID; /// One of MESSAGE_CONFIG_* values.
} message_config_read_t;

/** STOP workmode. Use it to disable sampling. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
} message_workmode_stop_t;

/** Starts freerunning workmode. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint32_t samples_count;
} message_workmode_freerunning_t;

/** Starts trigger-input workmode. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint32_t samples_count;
    uint32_t delay;
    uint8_t edge; /// One of edge_t values
} message_workmode_trigger_input_t;

/** Starts trigger-output workmode. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint32_t samples_count;
    uint32_t delay;
    uint32_t period;
    uint8_t edge; /// One of edge_t values
} message_workmode_trigger_output_t;

/** Starts simulation workmode. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint32_t samples_count;
    uint8_t sample_size; ///One of sample_size_t values
    float noise_rms;
    uint32_t period_in_ms;
    data_samples_t samples[];
} message_workmode_simulation_t;

/** Reads current workmode settings. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
} message_workmode_read_t;

/** Disables processing for one slot. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
} message_processing_none_t;

/** Configures processing slot to simple average algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
} message_processing_simple_average_t;

/** Configures processing slot to sample iir algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
    float weight;
} message_processing_sample_iir_t;

/** Configures processing slot to buffer iir algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
    float weight;
} message_processing_buffer_iir_t;

/** Configures processing slot to oversampling algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
    uint32_t ratio;
    uint32_t output_samples;
} message_processing_oversampling_t;

/** Configures processing slot to peak-peak detection algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
} message_processing_peak_peak_t;

/** Configures processing slot to buffer decimation algorithm. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t slotId;
    uint32_t ratio;
} message_processing_buffer_decimation_t;

/** Reads processing configuration. Check AMS-DIG-PROC datasheet for more information. */
typedef struct {
  MESSAGE_GENERIC_PART;
  uint8_t slotID;
} message_processing_read_t;

/** Message with samples acquired by the AMS-DIG-PROC board.*/
typedef struct {
    MESSAGE_GENERIC_PART;
    uint8_t counter; /** Will be incremented for each message. Check AMS-DIG-PROC datasheet for more information. */
    uint8_t sample_size; ///One of sample_size_t values
    union {
        uint8_t  data8[0];   /// Actual length of samples can be extracted from lower layer (COBS)
        uint16_t data16[0]; /// Actual length of samples can be extracted from lower layer (COBS)
        uint32_t data32[0]; /// Actual length of samples can be extracted from lower layer (COBS)
    };
} message_data_generic_t;

/** Represents all possible configuration messages. Check AMS-DIG-PROC datasheet for more information. */
typedef union {
    message_configure_sampling_t sampling;
    message_configure_communication_t communication;
    message_configure_detector_temperature_t detector_temperature;
    message_configure_user_space_t user_space;
} message_any_config_message_t;


/** Message ids. Check AMS-DIG-PROC datasheet for more information. */
typedef enum {
    MESSAGE_MODE_STOP = 3,
    MESSAGE_MODE_FREE_RUNNING = 5,
    MESSAGE_MODE_TRIGGER_INPUT = 6,
    MESSAGE_MODE_TRIGGER_OUTPUT = 7,
    MESSAGE_MODE_SIMULATION = 8,
    MESSAGE_PROCESSING_NONE = 9,
    MESSAGE_PROCESSING_SIMPLE_AVERAGE = 10,
    MESSAGE_PROCESSING_SAMPLE_IIR = 11,
    MESSAGE_PROCESSING_BUFFER_IIR = 12,
    MESSAGE_PROCESSING_OVERSAMPLING = 13,
    MESSAGE_PROCESSING_PEAK_PEAK = 14,
    MESSAGE_PROCESSING_BUFFER_DECIMATION = 15,
    MESSAGE_CONFIGURE_COMMUNICATION = 50,
    MESSAGE_CONFIGURE_SAMPLING = 51,
    MESSAGE_CONFIGURE_DETECTOR_TEMPERATURE = 52,
    MESSAGE_CONFIGURE_USER_SPACE = 53,
    MESSAGE_CONFIGURE_GAIN = 54,
    MESSAGE_CONFIG_SAVE = 55,
    MESSAGE_CONFIG_READ = 56,
    MESSAGE_OUTPUT_DATA = 90,
    MESSAGE_MODE_READ = 100,
    MESSAGE_PROCESSING_READ = 105,
    MESSAGE_STATUS = 120,
    MESSAGE_REBOOT = 124,
    MESSAGE_CLEAR_RESET_FLAG = 125,
} protocol_msg_id_t;

#pragma pack(pop)

#endif
