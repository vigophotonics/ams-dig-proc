Python API
==========

Several messages are defined in ams_messages module, however most of them are wrapped in methods inside ams_module module.
Recommended usage is:

  - Use AMSSerialModule class to send and receive messages
  - Use messages to read parameters and attributes read from the board

Check out the :doc:`examples` section for more information how to use API.


  .. automodule:: ams_module
     :members:
     :undoc-members:

  .. autoclass:: AMSSerialModule
     :members:
     :undoc-members:

  .. automodule:: ams_messages
     :members:
     :undoc-members:

  .. autoclass:: MessageOutputData
     :members:

  .. autoclass:: MessageStatus
     :members:

  .. autoclass:: AbstractWorkmode
     :members:

  .. autoclass:: MessageWorkmodeFreerunning
     :members:

  .. autoclass:: MessageWorkmodeStop
     :members:

  .. autoclass:: MessageWorkmodeTriggerInput
     :members:

  .. autoclass:: MessageWorkmodeTriggerOutput
     :members: