
# C examples

This examples assume, that there are some functions available:

- `cobsEncode` - encodes data into COBS frame. Example implementation can be found [here](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing#Implementation)
- `cobsDecode` - decodes data from COBS frame. Example implementation can be found [here](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing#Implementation)
- `serial_write(uint8_t * buffer, uint32_t length)` - writes buffer to serial port
- `size_t serial_read_until_zero(uint8_t * buffer, size_t buffer_length)` - reads data from serial port until zero character is received

## Checking crc for a message
``` c

#include <ams_dig_proc_crc.h>

int is_crc_valid(uint8_t * buffer, uint32_t msg_len) {
   message_header_t * header = (message_header_t *) buffer;
   uint32_t calculated_crc = ams_dig_proc_crc(&header->msgId, 0, msg_len - sizeof(header->crc), msg_len);
   if (calculated_crc == header->crc) {
      return 1;
   }
   return 0;
}
```

## Checking type of a message
``` c
int check_message_type(uint8_t * buffer, uint8_t msgId) {
   message_header_t * header = (message_header_t *) buffer;
   if (header->msgId == msgId) {
      return 1;
   }
   return 0;
}
```

## Waiting for status message and reading temperature of the detector

``` c
#include <protocol.h>
size_t msg_len;
uint8_t rx_buffer[10000];
while (1) {
   msg_len = serial_read_until_zero(rx_buffer, sizeof(rx_buffer));
   if (msg_len <= 0) {
      continue;
   }
   if (!is_crc_valid(rx_buffer, msg_len)) {
      printf("INVALID CRC\n");
      continue;
   }
   if (!check_message_type(rx_buffer, MESSAGE_STATUS)) {
      continue;
   }
   break;
}
message_status_t * status_msg = (message_status_t *) rx_buffer;
assert(sizeof(message_status_t) == msg_len);
printf("Got status message. Detector temperature = %d\n", status_msg->detector_temperature)
```

## Waiting for output samples

``` c
while (1) {
   msg_len = serial_read_until_zero(rx_buffer, sizeof(rx_buffer));
   if (msg_len <= 0) {
      continue;
   }
   if (!is_crc_valid(rx_buffer, msg_len)) {
      printf("INVALID CRC\n");
      continue;
   }
   if (!check_message_type(rx_buffer, MESSAGE_OUTPUT_DATA)) {
      continue;
   }
   break;
}
message_data_generic_t * data_msg = (message_data_generic_t *) rx_buffer;
uint32_t samples_count = (msg_len - sizeof(message_header_t) - sizeof(data_msg->counter) - sizeof(data_msg->sample_size)) / data_msg->sample_size;
switch (data_msg->sample_size) {
   case SAMPLE_SIZE_8b: {
      
      uint8_t * samples = data_msg->data8;
      // Do with samples whatever you want here. Keep in mind that value 128 means zero signal
      break;
   }
   case SAMPLE_SIZE_16b: {
      uint16_t * samples = data_msg->data16;
      // Do with samples whatever you want here. Keep in mind that value 32768 means zero signal
      break;
   }
   case SAMPLE_SIZE_32b: {
      uint32_t * samples = data_msg->data32;
      // Do with samples whatever you want here. Keep in mind that value 2^31 means zero signal
      break;
   }
   default: {
      printf("Unknown sample size = %d\n", data_msg->sample_size);
      break;
   }
}
```

## Disabling processing slots

``` c
int i;
for (i = 0; i < PROCESSING_SLOTS; i++) {
   message_processing_none_t msg;
   msg.header.msgId = MESSAGE_PROCESSING_NONE;
   msg.slotId = i;
   uint32_t calculated_crc = ams_dig_proc_crc(&msg->header.msgId, 0, sizeof(msg) - sizeof(msg.crc), sizeof(msg));
   msg.header.crc = calculated_crc;
   uint8_t encoded_buffer[SIZE_WITH_COBS_OVERHEAD(sizeof(msg))];
   size_t encoded_length = cobsEncode(&msg, sizeof(msg), encoded_buffer);
   serial_write(encoded_buffer, encoded_length);
}
```

## Setting temperature of the detector
``` c
message_configure_detector_temperature_t msg;
msg.header.msgId = MESSAGE_CONFIGURE_DETECTOR_TEMPERATURE;
msg.temperature = 273; // ~ 0degC
uint32_t calculated_crc = ams_dig_proc_crc(&msg->header.msgId, 0, sizeof(msg) - sizeof(msg.crc), sizeof(msg));
msg.header.crc = calculated_crc;
uint8_t encoded_buffer[SIZE_WITH_COBS_OVERHEAD(sizeof(msg))];
size_t encoded_length = cobsEncode(&msg, sizeof(msg), encoded_buffer);
serial_write(encoded_buffer, encoded_length);
```