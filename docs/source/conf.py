# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'ams-dig-proc'
copyright = '2023, Vigo Photonics'
author = 'psalek'

release = '1.0'
version = '1.0.0'

# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'myst_parser',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_css_files = ['mystyle.css']

# -- Options for EPUB output
epub_show_urls = 'footnote'
toc_object_entries = False

import sys, os

sys.path.insert(0, os.path.abspath('../../python'))
sys.path.insert(0, os.path.abspath('../..'))
sys.path.insert(0, os.path.abspath('../'))
sys.path.insert(0, os.path.abspath('../../python/ams_module'))
sys.path.insert(0, os.path.abspath('../python/ams_module'))
