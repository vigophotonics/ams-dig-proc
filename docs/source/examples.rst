Python examples
===============

Connecting to the board
-----------------------

.. code-block:: python

   from ams_messages import *
   from ams_module import AMSSerialModule
   module = AMSSerialModule(port="COM1")


Temperature setting
-------------------

.. code-block:: python

   module.workmode_stop()
   module.set_detector_temperature(263)
   module.save_config()


Waiting for temperature stabilization
-------------------------------------

.. code-block:: python

   module.wait_for_temperature_stabilization()



Signal measurement with mechanical chopper
------------------------------------------

.. code-block:: python

   module.workmode_stop()
   module.processing_disable()
   # By default AMS-DIG-PROC produces 7Msamples per second in 2048-samples buffers. 
   # This means that one single buffer takes approx. 0.3ms.
   # The chopper has usually 1kHz (period: 1ms) and we would like to capture couple periods.
   # Oversampling ratio can be calculated as 1ms / 0.3ms * N ~= 3*N
   # We choose N=10 periods, so ratio will be set to 32.
   module.processing_oversampling(0, 32, 2048)
   module.processing_buffer_iir(1, 0.95) # Adjust second parameter. The closer to 1, the better (and slower) averaging.
   module.workmode_trigger_input(samples_count=32*2048, delay=0)
   module.flush_input() # To avoid reading old messages
   while True:
      msg = module.read_one_message(msgClasses=MessageOutputData)
      if not msg:
         continue
      print(msg.voltages) # data contains all samples



Peak-peak measurements with mechanical chopper
----------------------------------------------

.. code-block:: python

   module.workmode_stop()
   module.processing_disable()
   module.processing_oversampling(0, 32, 2048)
   module.processing_buffer_iir(1, 0.95) # Adjust second parameter. The closer to 1, the better (and slower) averaging.
   module.processing_peak_peak(2)
   module.workmode_trigger_input(samples_count=32*2048, delay=0)
   module.flush_input() # To avoid reading old messages
   while True:
      msg = module.read_one_message(msgClasses=MessageOutputData)
      if not msg:
         continue
      print(msg.voltages[0])


Freerunning average voltage measurement with oversampling
---------------------------------------------------------

.. code-block:: python

   module.workmode_stop()
   module.processing_disable()
   module.processing_oversampling(0, 2048, 2048)
   module.processing_simple_average(1)
   module.workmode_freerunning()
   module.flush_input() # To avoid reading old messages
   while True:
      msg = module.read_one_message(msgClasses=MessageOutputData)
      if not msg:
         continue
      print(msg.voltages[0])


Resetting the board
-------------------

.. code-block:: python

   module.reset()


Clearing reset flag
-------------------

.. code-block:: python

   module.clear_reset_flag()


Checking wheter the board has rebooted
--------------------------------------

.. code-block:: python

   module.flush_input() # To avoid reading old messages
   while True:
      msg = module.read_one_message(msgClasses=MessageStatus)
      if msg:
          break
   print(msg.reset_flag)


   