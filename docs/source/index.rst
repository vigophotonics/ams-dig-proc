Welcome to AMS-DIG-PROC libraries documentation!
================================================

.. image:: ams-dig-proc.jpg
   :width: 200
   :alt: AMS-DIG-PROC board for Infrared Detection Modules by Vigo Photonics.


`AMS-DIG-PROC <https://vigophotonics.com/product/ams-dig-proc/>`_ board is an digital extension board to AMS family of `infrared detection modules <https://vigophotonics.com/products/infrared-detection-modules/inassb-affordable-detection-modules/>`_ .

This board provides 7Msamples/s 16-bit data aquisition and 32-bit real time processing capabilities. Onboard processing reduces data rate on the
communication link, therefore simple 1Mbit UART is sufficient in most applications. There is also `USB adapter <https://vigophotonics.com/product/ams-dig-usb/>`_ available, providing power supply and communication interface over a single micro-USB connector.


This documentation describes python and C libraries from the `repository of AMS-DIG-PROC <https://gitlab.com/vigophotonics/ams-dig-proc>`_ board.

For more detailed information about the board and the communication protocol please check the `datasheet of AMS-DIG-PROC board <https://vigophotonics.com/products/infrared-detection-modules/accessories/accessories-to-the-ams-detection-module-series/>`_ .

For more information about the hardware visit `AMS-DIG-PROC product page <https://vigophotonics.com/product/ams-dig-proc/>`_ .


Python or C?
------------
For Windows/Linux - Python API is recommended. It provides ctypes structs and methods to cast and calculate CRC. Also serial port management and separate reader thread is provided to prevent from blocking the main thread.

For other operating systems or bare metal embedded aplications - C is recommended since it does not introduce any dependencies or specific requirements.
It is designed to be portable and as simple as possible.


How about Java/Android?
-----------------------
If you are going to implement your software on Android please take a look at `this repository <https://gitlab.com/vigophotonics/ams-dig-proc-for-android/>`_ .


Contents
--------

.. toctree::
   :maxdepth: 1
   :titlesonly:

   installation
   examples
   c_examples
   api
