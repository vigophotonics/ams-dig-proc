Installation
============

Python
-----------

To use AMS-DIG-PROC board, first install its library using pip:
```
pip install ams-dig-proc
```

C
-----------
There is a header file available in the projects [repository](https://gitlab.com/vigophotonics/ams-dig-proc/-/tree/main/C/Inc)
It contains definitions of all messages required to work with AMS-DIG-PROC board.

Additionally, [repository](https://gitlab.com/vigophotonics/ams-dig-proc/-/tree/main/C) contains function that can be used for CRC calculations.

There are no special requirements or installation procedure for C source files.